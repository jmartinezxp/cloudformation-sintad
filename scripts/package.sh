zip -r cloudformation/customs/build/ssm/put-properties-file.zip cloudformation/customs/src/ssm/put-properties-file
zip -r cloudformation/customs/build/efs/tranfer-from-s3.zip cloudformation/customs/src/efs/tranfer-from-s3

date=`date +%Y-%m-%d-%H-%M-%S`
bucket="migracion-sintad-template"
prefix="sintad/facturacion/$date"
outputfile="packaged-template.yaml"

aws cloudformation package \
--template-file cloudformation/migracion.yaml  \
--s3-bucket $bucket \
--output-template-file packaged/$outputfile

aws s3 cp packaged/$outputfile s3://$bucket/$prefix/$outputfile

echo "https://$bucket.s3.amazonaws.com/$prefix/$outputfile"