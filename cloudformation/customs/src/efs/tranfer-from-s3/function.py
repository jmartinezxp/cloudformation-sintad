import os
import boto3
from crhelper import CfnResource
import logging
import traceback
import botocore
import shutil

logger = logging.getLogger(__name__)

helper = CfnResource(
    json_logging=False,
    log_level='DEBUG',
    boto_level='CRITICAL'
)
s3 = boto3.resource('s3')


def loadToEfs(event, context):
    resource_properties = event["ResourceProperties"]
    stack_id = event["StackId"]
    request_type = event["RequestType"]

    _mount_path = os.environ["EFS_MOUNT_PATH"]
    _efs_applications_path = os.environ["EFS_ROOT_PATH"]

    _application_path = resource_properties["ApplicationPath"]
    _bucket = os.environ["S3_BUCKET"] #resource_properties["S3Bucket"]
    _prefix = resource_properties["S3Prefix"]

    if not _application_path.startswith(_efs_applications_path):
        raise Exception(f'Ahe application path should start with {_efs_applications_path}')

    _path = f"{_mount_path}{_application_path.replace(_efs_applications_path,'',1)}"

    logger.info(f"creating application path {_path}")
    os.makedirs(_path, exist_ok=True)
    logger.info(f"application path {_path} created")

    bucket = s3.Bucket(_bucket)

    keys = [(obj.key, obj.e_tag) for obj in bucket.objects.filter(Prefix=_prefix)]

    for key in keys:
        if key[0].endswith("/"):
            continue
        print(f"key: {key[0]}")
        __path = os.path.join(_path, key[0].replace(_prefix, "", 1))
        os.makedirs(os.path.dirname(__path), exist_ok=True)
        print(f"Downloading {key[0]} to {__path}")
        bucket.download_file(key[0], __path)


def handler(event, context):
    helper(event, context)


@helper.create
def create(event, context):
    loadToEfs(event, context)
    logger.info("Got Create")


@helper.update
def update(event, context):
    loadToEfs(event, context)
    logger.info("Got Update")


@helper.delete
def delete(event, context):
    try:
        resource_properties = event["ResourceProperties"]
        stack_id = event["StackId"]
        request_type = event["RequestType"]

        _mount_path = os.environ["EFS_MOUNT_PATH"]
        _efs_applications_path = os.environ["EFS_ROOT_PATH"]
        _application_path = resource_properties["ApplicationPath"]
        _path = f"{_mount_path}{_application_path.replace(_efs_applications_path,'',1)}"
        logger.info(f"deleting {_path}")
        shutil.rmtree(_path)
        logger.info("Got Delete")

    except:
        logger.error(traceback.format_exc())
